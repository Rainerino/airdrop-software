#include <Arduino.h>
#include "Encoder.h"
#include "Ticker.h"
#include <cstdlib>

Encoder uas_encoder(3, 4);
uint32_t encoder_cur_tick;
uint32_t encoder_prev_tick;

uint16_t ENCODER_TICK_PER_RES = 81; // ticks.
uint16_t DRUM_R = 23; // mm
const uint8_t  DRUM_2_ENCODER_GEAR_RATIO_X_10 = 14;
const double ENCODER_MM_PER_RES  = 2* M_PI * DRUM_R; // 439 ->439.82 / 2
const uint16_t ENCODER_MM_PER_TICK_X_1000 =  uint32_t((1000* ENCODER_MM_PER_RES / ENCODER_TICK_PER_RES) *
        DRUM_2_ENCODER_GEAR_RATIO_X_10/10) ; // 565-> 7587->7601/2, should be 542.9913 * 1.4 = 7.606 -> 7606/2
const uint8_t SPEED_DELTA_T = 100; //ms


uint32_t current_speed; //mm/s




uint32_t encoder_total_distance(Encoder uas_encoder){
    // encoder_cur_tick = uint32_t(abs(uas_encoder.read()));
    return uint32_t(encoder_cur_tick * ENCODER_MM_PER_TICK_X_1000 / 1000.0);
}

uint16_t encoder_tick_diff(Encoder uas_encoder){
    encoder_cur_tick = uint32_t(abs(uas_encoder.read()));
    uint16_t difference = abs(encoder_cur_tick - encoder_prev_tick);
    encoder_prev_tick = encoder_cur_tick;
    return difference;
}

void encoder_update_current_speed(uint8_t delta_t, Encoder uas_encoder){
    uint16_t difference = encoder_tick_diff(uas_encoder);
    if (difference!= 0) {
        Serial.println(difference);
        current_speed = uint32_t((difference * ENCODER_MM_PER_TICK_X_1000 / delta_t)); // mm/s
    }else{
        // avoid 0 division
        current_speed = 0;
    }
}
void calculate_speed(){
    encoder_update_current_speed(SPEED_DELTA_T, uas_encoder);
    // Give out warning if the current speed exceeds the rated speed, that means encoder is likely to be
    // missing steps and requires some restart or manual control.
    // driver.encoder_valid(SPEED_DELTA_T);
}
Ticker encoder_speed(calculate_speed, SPEED_DELTA_T, 0, MILLIS); // update speed at certain rate


void setup() {
  // put your setup code here, to run once:
  encoder_speed.start();
  Serial.begin(9600);
  encoder_cur_tick = 0;
  encoder_prev_tick = 0;
  current_speed = 0;
}

void loop() {
  encoder_speed.update();

  delay(300);
  Serial.printf("Current Speed: %d \n", current_speed);
  Serial.printf("Distance: %ld\n", encoder_total_distance(uas_encoder));
  Serial.printf("====Curret Tick: %ld\n", encoder_cur_tick);
  // Serial.printf("====Prev Tick: %ld\n", encoder_prev_tick);
  Serial.printf("READING: %ld\n", uas_encoder.read());
  // put your main code here, to run repeatedly:
}