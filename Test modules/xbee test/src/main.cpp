#include <Arduino.h>

#include <uas_comm.h>
#include <Encoder.h>
#include <PWMServo.h>
#include <Ticker.h>

comm::UASComm * uas_comm;

void commCallback(){
  uas_comm->commUpdateSignal();
}

Ticker commTick(commCallback, 500, 0);
void setup() {
  // put your setup code here, to run once:
  uas_comm = new comm::UASComm(comm::System::ROVER_SYSTEM);
  commTick.start();
}

void loop() {
  // put your main code here, to run repeatedly:
  commTick.update();
}