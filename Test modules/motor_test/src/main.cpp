#include <Arduino.h>

#define motor_in1 21
#define motor_in2 20
void setup() {
  // put your setup code here, to run once:
    pinMode(motor_in1, OUTPUT);
    pinMode(motor_in2, OUTPUT);
    pinMode(13, HIGH);
    digitalWrite(motor_in1, LOW);
    digitalWrite(13, HIGH);
    Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(motor_in2, 90);
  Serial.printf("?");
}