// Globals.cpp

#include "Globals.h"
uint8_t GPS_PIN_1 = 0;
uint8_t GPS_PIN_2 = 1;



uint8_t TRIG_PIN = 3;
uint8_t ECHO_PIN = 4;

uint8_t LEFT_MOTOR_IN1 = 5;
uint8_t LEFT_MOTOR_IN2 = 6;
uint8_t RIGHT_MOTOR_IN1 = 7;
uint8_t RIGHT_MOTOR_IN2 = 8;

uint8_t SERVO_PIN = 9;

uint8_t STAGE_1_TRIG_PIN = 10;



//UBC testing
double GOAL_LAT = 49.262229;
double GOAL_LON =  -123.248473;

// UBC field
// double GOAL_LAT = 49.259322;
// double GOAL_LON = -123.241328;

double TURN_ANGLE_MIN = 10; // in degree
double GOAL_STOP_DISTANCE_RADIUS = 10; // in unit of meters

uint16_t MOTOR_SPEED_CAP = 255;




uint8_t SERVO_CLOSED = 35;
uint8_t SERVO_STAGE_1 = 105;
uint8_t SERVO_STAGE_2 = 160;
uint8_t SERVO_DELAY = 100;

uint32_t GPS_BAUD = 9600;


uint16_t MINI_TRIG_DISTANCE = 5;

uint64_t LOOP_DELAY_1 = 100;
uint64_t LOOP_DELAY_2 = 100;
uint64_t LOOP_DELAY_3 = 1000;
uint64_t LOOP_IDLE = 200;
uint64_t GPS_DELAY = 1000;
