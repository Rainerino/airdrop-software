#ifndef UAS_COMM_H
#define UAS_COMM_H

#include <globals.h>

namespace comm{
    enum class Flags:int {
        IDLE = 0,
        ROVER_RELEASED = 1, 
        ROVER_NEAR_GROUND = 2, 
        WINCH_DROP_TRIGGERED = 4,
    };
    enum class System :int {
        WINCH_SYSTEM = 1, 
        ROVER_SYSTEM
    };

    inline Flags operator|=( Flags a,  Flags b){
        return static_cast<Flags>(static_cast<int>(a) | static_cast<int>(b));
    }

    inline Flags operator+=(Flags a, Flags b){
        return static_cast<Flags>(static_cast<int>(a) + static_cast<int>(b));
    }
    inline Flags operator^(Flags a, Flags b){
        return static_cast<Flags>(static_cast<int>(a) ^ static_cast<int>(b));
    }


    inline bool operator&(Flags a, Flags b){
        return static_cast<int>(a) & static_cast<int>(b);
    }
    inline bool operator&(int a, Flags b){
        return a & static_cast<int>(b);
    }

    inline bool operator==(Flags a, int b){
        return static_cast<int>(a) == b;
    }

    inline bool operator==(int a, Flags  b){
        return a == static_cast<int>(b);
    }
    inline bool operator==(char a, Flags b){
        return a == static_cast<int>(b);
    }


    class UASComm{
        public:
            UASComm();
            UASComm(System system_type);

            /**
             * TODO this function should both read and write!!!
             * 
             */ 
            void commUpdateSignal();

            void completeStage2Release();

            void triggerApproachGround();

            Flags current_signal;

            void setRoverNearGroundStage();

            void setRoverReleasedStage();

            void winchDropTriggerStage();

            void serialDebug();



        private:
            void receiveSerialData();
            void sendSerialData();
            System current_system;
            bool rover_near_ground_stage;
            bool rover_released_stage;
            bool winch_drop_trigger_stage;

            char input;
    };
};



#endif