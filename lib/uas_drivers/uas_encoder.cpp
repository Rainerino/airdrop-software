#include <uas_encoder.h>

namespace encoder{
    UASEncoder::UASEncoder(uint8_t pin_A, uint8_t pin_B){
        encoder = new Encoder(pin_A, pin_B);
        encoder_cur_tick = 0;
        encoder_prev_tick = 0;
        current_speed = 0;

        // TODO: FIX DIRECTION!!! USe delta to see the direction
        direction = Direction::DOWN;

        current_distance = 0;
    }

    UASEncoder::~UASEncoder(){
        delete encoder;
    }

    uint16_t UASEncoder::encoderTotalDistance(){
        this->current_distance = uint16_t(encoder_cur_tick * global::ENCODER_MM_PER_TICK_X_1000 / 1000.0);
        return current_distance;
    }

    uint16_t UASEncoder::encoderTickDiff(){
        encoder_cur_tick = uint32_t(abs(encoder->read()));
        if (encoder_cur_tick - encoder_prev_tick > 0 ){
            direction = Direction::DOWN;
        }else{
            direction = Direction::UP;
        }
        uint16_t difference = abs(encoder_cur_tick - encoder_prev_tick);
        encoder_prev_tick = encoder_cur_tick;
        return difference;
    }

    uint16_t UASEncoder::encoderUpdateCurrentSpeed(uint8_t delta_t){

        uint16_t difference = this->encoderTickDiff();
        if (difference!= 0) {
            current_speed = uint32_t((difference * global::ENCODER_MM_PER_TICK_X_1000 / delta_t)); // mm/s
        }else{
            // avoid 0 division
            current_speed = 0;
        }
        return current_speed;
    }

    void UASEncoder::encoderSerialDebug(){
        Serial.printf(F("=============Encoder Debug ===========\n"));
        Serial.printf(F("current param: %d\n"), global::ENCODER_MM_PER_TICK_X_1000);
        Serial.printf(F("current tick: %d\n"), encoder_cur_tick);
        Serial.printf(F("actual tick: %d\n"), encoder->read());
        Serial.printf(F("total distance: %d mm\n"), current_distance);
        Serial.printf(F("Direction is %d (up is 1)\n"), direction);
        Serial.printf(F("current speed: %d mm/s\n\n"), current_speed);
    }

    void UASEncoder::encoderReset(){
        encoder->write(0);
        encoder_cur_tick = 0;
        encoder_prev_tick = 0;
        current_distance = 0;
        current_speed = 0;
    }

};