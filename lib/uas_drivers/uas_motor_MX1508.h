#ifndef UAS_MOTOR_MX1508_H
#define UAS_MOTOR_MX1508_H

#include <globals.h>

// https://www.amazon.ca/Robojax-H-Bridge-Stepper-MX1508-Arduino/dp/B07CW42MJV/ref=sr_1_30?keywords=motor+driver&qid=1560094801&s=gateway&sr=8-30

namespace motor{
    class UASMotor{
        private:
            uint8_t motor_in1_pin;
            uint8_t motor_in2_pin;  
            uint8_t motor_controll_pin;
            uint8_t motor_direction_pin;
            uint8_t motor_enA_pin;

        public:

            UASMotor(uint8_t motor_in1, uint8_t motor_in2, uint8_t enA);
            /**
             *
             * @param percent
             */
            void motor_run_at(int16_t percent);    

            void motor_reverse_direction(); 

            void motorSerialDebug();      

            bool motor_direction;
            
            int8_t motor_percent;
    };

};

#endif 