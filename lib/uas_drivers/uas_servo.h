#ifndef UAS_SERVO_H
#define UAS_SERVO_H

#include <PWMServo.h>

namespace servo{
    // enum system {WINCH, ROVER};
    class UASServo{
        private:
            uint8_t servo_pin;
        public:
            UASServo(uint8_t servo_pin);
            void servo_brake_range(uint8_t low, uint8_t high);

            // ==== Winch 

            void servo_release();
            void servo_brake();

            // === Rover
            
    };

};



#endif 