#include <uas_comm.h>

namespace comm{

    UASComm::UASComm(System system_type){
        current_signal = Flags::IDLE;
        Serial3.begin(global::BAUD_RATE);
        current_system = system_type;
        rover_near_ground_stage = false;
        rover_released_stage = false;
        winch_drop_trigger_stage = false;
    }
    void UASComm::commUpdateSignal(){
        receiveSerialData();
        sendSerialData();
        // serialDebug();
    }

    void UASComm::receiveSerialData(){
        // TODO: Finish this up
        if(Serial3.available()){
            input = Serial3.read();

            if (current_system == System::WINCH_SYSTEM){
                if ((input - '0') == Flags::ROVER_NEAR_GROUND){
                    current_signal =  Flags::ROVER_NEAR_GROUND;
                }else if((input - '0') == Flags::ROVER_RELEASED ){
                    current_signal = Flags::ROVER_RELEASED;
                }
            }else if (current_system == System::ROVER_SYSTEM){
                if ((input - '0') == Flags::WINCH_DROP_TRIGGERED){
                    current_signal = Flags::WINCH_DROP_TRIGGERED;
                }
            }
        }

    }
    void UASComm::setRoverNearGroundStage(){
        rover_near_ground_stage = true;
    }

    void UASComm::setRoverReleasedStage(){
        rover_released_stage = true;
    }
    void UASComm::winchDropTriggerStage(){
        winch_drop_trigger_stage = true;
    }

    void UASComm::sendSerialData(){
        if (current_system == System::WINCH_SYSTEM){
            if(winch_drop_trigger_stage){
                current_signal = Flags::WINCH_DROP_TRIGGERED;
                winch_drop_trigger_stage = false;
                Serial3.printf("%d", current_signal);
            }
        }else if (current_system == System::ROVER_SYSTEM){
            if(rover_near_ground_stage){
                current_signal = Flags::ROVER_NEAR_GROUND;
                Serial3.printf("%d", current_signal);
            }else if(rover_released_stage){
                current_signal = Flags::ROVER_RELEASED;
                Serial3.printf("%d", current_signal);
            }
        }
    }
    void UASComm::serialDebug(){
        Serial.printf("========== COMM ===== \n");
        Serial.printf("current signal: %d, input is: %c, sys %d\n\n", current_signal, input, current_system);
    }
};