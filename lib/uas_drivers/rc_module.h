#ifndef RC_MODULE_H
#define RC_MODULE_H

#include <globals.h>

namespace rc{

    // ==== RC module const =============

    // ==== RC module const =============
    
    class RCModule{
        public: 
        struct rc_channel_input{
            uint8_t pin;
            uint16_t raw_value; // just in case
            bool trigger; // save the trigger value. True to triggered
            uint8_t mode; // to save mode
            bool change; // for detecting mode changes
            int8_t percentage; // value out of 100, for speed control
        }rc_winch_servo, rc_winch_motor, rc_winch_mode, rc_winch_auto_mode_trigger;
        

        RCModule();
        /**
         * set up input pins
         * 
         */ 
        void attachRCPin(rc_channel_input& input, uint8_t rc_pin);

        /**
         * 
         */ 
        bool rcWinchMotorReading(); // if fucntion doest change member variables, use const
 
        bool rcWinchServoReading();

        /**
         * 
         * 
         */ 
        bool rcWinchModeReading();

        /**
         * 
         * 
         */ 
        bool rcWinchAutoModeTriggerReading();

        /**
         * 
         * 
         */ 
        bool rcReadValidInput(rc_channel_input * rc_input);

        /**
         * 
         * 
         */ 
        void rcSerialDebug();
        
        private: 
    };
};


#endif 