#include <uas_motor_L298N.h>
#include <stdlib.h>

namespace motor{
    UASMotor::UASMotor(uint8_t motor_in1, uint8_t motor_in2, uint8_t enA){
        motor_in1_pin = motor_in1;
        motor_in2_pin = motor_in2;
        motor_enA_pin = enA;

        pinMode(motor_in1_pin, OUTPUT);
        pinMode(motor_in2_pin, OUTPUT);
        pinMode(motor_enA_pin, OUTPUT);

        motor_controll_pin = motor_in1_pin;

        digitalWrite(motor_enA_pin, LOW);
        motor_direction = true;
        motor_percent = 0;
    }
    void UASMotor::motor_run_at(int16_t percent){
        motor_percent = percent;
        if (motor_percent == 0){
            analogWrite(motor_controll_pin, 0);
            digitalWrite(motor_enA_pin, LOW);
        }else{
            digitalWrite(motor_enA_pin, HIGH);

            if (motor_percent > 0 && motor_direction == true){
                motor_reverse_direction();
            }else if (motor_percent < 0 && motor_direction == false){
                motor_reverse_direction();
            }
            analogWrite(motor_controll_pin, uint8_t(map(abs(motor_percent), 0, 100 ,0, 255)));
        }
    }
    void UASMotor::motor_reverse_direction(){
        motor_direction = !motor_direction;
        if (motor_direction){
            digitalWrite(motor_in2_pin, LOW); //HIGH if releasing the rope, LOW if realing the rope
            motor_controll_pin = motor_in1_pin;
        }else{
            digitalWrite(motor_in1_pin, LOW); //LOW if releasing the rope, HIGH if realing the rope
            motor_controll_pin = motor_in2_pin;
        }
    }
    void UASMotor::motorSerialDebug(){
        Serial.printf(F("===========Motor Debug ===========\n"));
        Serial.printf(F("motor speed at %d%\n"), motor_percent);
        Serial.printf(F("motor direction %d \n\n"), motor_direction);
    }
};
