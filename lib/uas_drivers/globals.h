#include <stdint.h>
#include <math.h>
#include <stdlib.h>

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

// #define SERVO_PIN 2

#define A_SIGNAL 2 //Pin number for encoder
#define B_SIGNAL 3 //Pin munber for encoder
// #define enA 14
#define IN1_PIN 6
#define IN2_PIN 5
#define ENA_PIN 4

#define RC_WINCH_MOTOR_PIN 23
#define RC_WINCH_TRIGGER_PIN 22
#define RC_WINCH_MODE_PIN 21
// #define CTRL_MODE_PIN 20

// Serial1
// #define XBEE_RX_PIN 0
// #define XBEE_TX_PIN 1 
// Serial2
// #define XBEE_RX_PIN 9
// #define XBEE_TX_PIN 10 
// Serial3
#define XBEE_RX_PIN 7
#define XBEE_TX_PIN 8 
// // Serial4 (3.6 only)
// #define XBEE_RX_PIN 31
// #define XBEE_TX_PIN 32

#define UAS_DEBUG 1

namespace global{
    // ======================================== WINCH ======================================//
    extern const uint16_t MANUAL_DELTA_T;

    // ============ AUTO mode related ===========
    extern const uint16_t AUTO_IDLE_DELTA_T;
    extern const uint16_t AUTO_RELEASE_DELTA_T;
    extern const uint16_t AUTO_RETRACT_DELTA_T;
    extern const uint16_t AUTO_POST_MISSION_IDLE_DELTA_T;

    extern const uint16_t AUTO_RELEASE_TRIGGER_WAIT_TIME;

    extern const double K_P_RETRACT;
    extern const double K_I_RETRACT ;
    extern const double K_D_RETRACT;

    extern const double K_P_RELEASE;
    extern const double K_I_RELEASE ;
    extern const double K_D_RELEASE;

    extern const double I_MAX;
    extern const double I_MIN;
    extern const double INITIAL_PERCENT;
    extern const double INITIAL_RELEASE_PERCENT;
    extern const double RELEASE_STALL_PERCENT;
    extern const double RETRACT_STALL_PERCENT;

    extern const int RELEASE_SLOW_DOWN_DISTANCE;
    extern const uint8_t POST_MISSION_IDLE_PERCENT;
    // ============ AUTO mode related  ===========

    // == Timing Related variables ===
    extern const int LOOP_SPEED ; //ms updating at 1/50*10^3 = 20Hz
    extern const int RC_DELTA_T ; // ms
    extern const int COMM_DELTA_T;
    // == Timing Related variables ===


    extern const double DESIRED_RELEASE_SPEED; //1.5m/s, 1500mm/s
    extern const double DESIRED_SLOW_RELEASE_SPEED;
    extern const double DESIRED_RETRACT_SPEED;

    extern const int MOTOR_LOW ;
    extern const int MOTOR_HIGH ;

    extern const uint16_t RC_CUTOFF_BAND ;

    extern const uint8_t RC_DEAD_BAND ;

    extern const uint16_t RC_MAX_BAND ;

    extern const uint16_t RC_MIN_BAND ;

    extern const uint16_t RC_MID_LOW_BAND ;

    extern const uint16_t RC_MID_HIGH_BAND ;

    // extern const uint16_t COMM_DELTA_T;

    extern const uint16_t BAUD_RATE;

    // =================== Encoder ==============
    extern const uint16_t ENCODER_TICK_PER_RES ; // ticks.
    extern const uint16_t DRUM_R ; // mm
    extern const uint8_t  DRUM_2_ENCODER_GEAR_RATIO_X_10;
    extern const double ENCODER_MM_PER_RES; // 439 ->439.82 / 2
    extern const uint16_t ENCODER_MM_PER_TICK_X_1000 ; // 565-> 7587->7601/2, should be 542.9913 * 1.4 = 7.606 -> 7606/2
    extern const uint8_t SPEED_DELTA_T ; //ms
    // =================== Encoder ==============


    // ======================================== WINCH ======================================//
    
};

