#include <rc_module.h>

namespace rc{

    RCModule::RCModule(){
    }

    void RCModule::attachRCPin(rc_channel_input & input, uint8_t rc_pin){
        pinMode(rc_pin, INPUT);
        input.pin = rc_pin;
        input.raw_value = 0;
        input.trigger = false;
        input.mode = 0;
        input.change = false;
        input.percentage = 0;
    }

    bool RCModule::rcWinchMotorReading(){
        if (this->rcReadValidInput( & this->rc_winch_motor)){
            
            if (this->rc_winch_motor.raw_value > global::RC_CUTOFF_BAND + global::RC_DEAD_BAND){
                // release
                this->rc_winch_motor.raw_value = constrain(this->rc_winch_motor.raw_value, global::RC_CUTOFF_BAND + global::RC_DEAD_BAND, global::RC_MAX_BAND);
                this->rc_winch_motor.percentage = int8_t(map(this->rc_winch_motor.raw_value, global::RC_CUTOFF_BAND, global::RC_MAX_BAND, 0, 100));

            }else if(this->rc_winch_motor.raw_value < global::RC_CUTOFF_BAND - global::RC_DEAD_BAND){

                this->rc_winch_motor.raw_value = constrain(this->rc_winch_motor.raw_value, global::RC_MIN_BAND, global::RC_CUTOFF_BAND - global::RC_DEAD_BAND);
                this->rc_winch_motor.percentage = int8_t(map(this->rc_winch_motor.raw_value, global::RC_MIN_BAND, global::RC_CUTOFF_BAND, 0, 100)) - 100;
            }else{
                this->rc_winch_motor.percentage = 0;
            }
            return true;
        }else{
            return false;
        }
    }

    bool RCModule::rcWinchServoReading(){

        return false;
    }

    bool RCModule::rcWinchModeReading(){
        if (this->rcReadValidInput(& this->rc_winch_mode)){
            if (this->rc_winch_mode.raw_value < global::RC_MID_LOW_BAND){
                this->rc_winch_mode.mode = 0;
            }else if(this->rc_winch_mode.raw_value > global::RC_MID_HIGH_BAND){
                this->rc_winch_mode.mode = 2;
            }else{
                this->rc_winch_mode.mode = 1;
            }
            return true;
        }else{
            return false;
        }        
    }

    bool RCModule::rcWinchAutoModeTriggerReading(){
        if (this->rcReadValidInput(& this->rc_winch_auto_mode_trigger)){
            if (this->rc_winch_auto_mode_trigger.raw_value < global::RC_CUTOFF_BAND){
                this->rc_winch_auto_mode_trigger.trigger = false;
            }else{
                if (!this->rc_winch_auto_mode_trigger.trigger){
                    this->rc_winch_auto_mode_trigger.change = true;
                }
                this->rc_winch_auto_mode_trigger.trigger =  true;
            }
            return true;
        }else{
            return false;
        }
    }

    bool RCModule::rcReadValidInput(rc_channel_input* rc_input){
        // TODO Use http://www.camelsoftware.com/2015/12/25/reading-pwm-signals-from-an-rc-receiver-with-arduino/ 
        rc_input->raw_value = pulseIn(rc_input->pin, HIGH);
        if (rc_input->raw_value == 0){
            return false;
        }
        return true;
    }

    void RCModule::rcSerialDebug(){
        Serial.printf(F("=========RC Debug==============\n"));
        Serial.printf(F("winch motor input: %d | current speed: %d\n"), this->rc_winch_motor.raw_value, this->rc_winch_motor.percentage);
        Serial.printf(F("winch mode input: %d | current mode: %d\n"), this->rc_winch_mode.raw_value, this->rc_winch_mode.mode);
        Serial.printf(F("winch trigger input: %d | trigger: %d | change: %d\n\n"), this->rc_winch_auto_mode_trigger.raw_value, this->rc_winch_auto_mode_trigger.trigger, rc_winch_auto_mode_trigger.change);
    }

    
};