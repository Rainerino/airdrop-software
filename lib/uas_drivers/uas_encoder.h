#ifndef UAS_ENCODER_H
#define UAS_ENCODER_H

#include <globals.h>
#include <cstdlib> // need this! Arduino abs doesn't work for some reason (p)
#include <Encoder.h>

namespace encoder{

    // follow the delta of distance, the rope, which points downwards
    enum class Direction {UP = 1, DOWN};
    // == Encoder module variables ===
    // == Encoder module variables ===

    class UASEncoder{
        public:

            UASEncoder(uint8_t pin_A, uint8_t pin_B);

            ~UASEncoder();

            uint16_t encoderTotalDistance();

            uint16_t encoderTickDiff();

            uint16_t encoderUpdateCurrentSpeed(uint8_t delta_t);

            void encoderSerialDebug();

            void encoderReset();
            
            Direction direction;

            Encoder* encoder;
            
        private:

            uint16_t current_speed; //mm/s

            uint16_t encoder_cur_tick;

            uint16_t encoder_prev_tick;

            int16_t current_distance;

    };
}


#endif