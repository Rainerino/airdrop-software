#include "globals.h"

namespace global{

        const uint16_t MANUAL_DELTA_T = 50;

        const uint16_t AUTO_IDLE_DELTA_T = 500;
        const uint16_t AUTO_RELEASE_DELTA_T = 200;
        const uint16_t AUTO_RETRACT_DELTA_T = 100;

        const uint16_t AUTO_POST_MISSION_IDLE_DELTA_T = 1000;
        const uint16_t AUTO_RELEASE_TRIGGER_WAIT_TIME = 200;

        const double K_P_RETRACT = 0.01;//0.03 (very jump, stop )
        const double K_I_RETRACT = 1.0;
        const double K_D_RETRACT = 0;//0.001

        const double K_P_RELEASE = 0.01;
        const double K_I_RELEASE = 1.0;
        const double K_D_RELEASE = 0.0005;

        const double INITIAL_PERCENT = 50;
        const double RELEASE_STALL_PERCENT = 35;
        const double RETRACT_STALL_PERCENT = 15;
        const int RELEASE_SLOW_DOWN_DISTANCE = 20000;
        const uint8_t POST_MISSION_IDLE_PERCENT = 8;

        const double I_MAX = 200;
        const double I_MIN = -200;


        const int LOOP_SPEED = 50; //ms updating at 1/50*10^3 = 20Hz
        const int RC_DELTA_T = 50; // ms
        const int COMM_DELTA_T = 300;

        // =============================== 
        const double DESIRED_RELEASE_SPEED = 1000; //1.5m/s, 1500mm/s
        const double DESIRED_SLOW_RELEASE_SPEED = 100;
        const double DESIRED_RETRACT_SPEED = 1300;// 1600

        const int MOTOR_LOW = 100;
        const int MOTOR_HIGH = 255;

        
        const uint16_t RC_CUTOFF_BAND = 1500;

        const uint8_t RC_DEAD_BAND = 30;

        const uint16_t RC_MAX_BAND = 1990;

        const uint16_t RC_MIN_BAND = 1000;

        const uint16_t RC_MID_LOW_BAND = 1200;

        const uint16_t RC_MID_HIGH_BAND = 1700;

        // const uint16_t COMM_DELTA_T = 500;
        
        const uint16_t BAUD_RATE = 9600;

        // =================== Encoder ==============
        const uint16_t ENCODER_TICK_PER_RES = 81; // ticks.
        const uint16_t DRUM_R = 17.5; // mm
        const uint8_t  DRUM_2_ENCODER_GEAR_RATIO_X_10 = 10;
        const double ENCODER_MM_PER_RES  = 2* M_PI * DRUM_R; // 439 ->439.82 / 2
        const uint16_t ENCODER_MM_PER_TICK_X_1000 =  uint32_t((1000* ENCODER_MM_PER_RES / ENCODER_TICK_PER_RES) *
        DRUM_2_ENCODER_GEAR_RATIO_X_10/10) ; // 565-> 7587->7601/2, should be 542.9913 * 1.4 = 7.606 -> 7606/2

        const uint8_t SPEED_DELTA_T = 100; //ms
        // =================== Encoder ==============
};
