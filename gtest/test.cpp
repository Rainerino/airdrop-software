#include <iostream>
#include <gtest/gtest.h>

#include "Arduino.h"
#include "UAS-Rover/src/Globals.h"
// g++ test.cpp -lgtest -lgtest_main -pthread --std=c++11 

//TODO: CMae testing or make

using namespace std;

int main(int argc, char** argv) {

  testing::InitGoogleTest(&argc, argv);
  
  return RUN_ALL_TESTS();
}