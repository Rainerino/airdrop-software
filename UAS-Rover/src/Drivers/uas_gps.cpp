#include "Drivers/uas_gps.h"
#include "SoftwareSerial.h"

UAS_gps::UAS_gps(){
  //SoftwareSerial gps_connection((int)GPS_PIN_1, (int)GPS_PIN_2);
  //serial_connection = SoftwareSerial((int)GPS_PIN_1, (int)GPS_PIN_2);
  //serial_connection = &gps_connection;
  //serial_connection->begin(9600);
  
  //TinyGPSPlus gps;
}
// static void smartDelay(unsigned long ms)
// {
//   unsigned long start = millis();
//   do
//   {
//     while (Serial1.available())
//       gps.encode(Serial1.read());
//   } while (millis() - start < ms);
// }

UAS_gps::gpsReading UAS_gps::get_current_gps_location(){
  // FIXME: Well this need to be finished
  current_position.lat = gps.location.lat();

  current_position.lon = gps.location.lng();
  current_position.alt = gps.altitude.meters();
  smartDelay(GPS_DELAY);
  // Serial.println("++++++++++++++++");
  //Serial.println(gps.location.lat());
  // Serial.println(current_position.lat);
  return current_position;
}



/*************************************************************************
 * //Function to calculate the distance between two waypoints
 *************************************************************************/
float UAS_gps::calculateDistanceBetweenGPSCoordinate(float flat1, float flon1, float flat2, float flon2)
{
  float dist_calc=0;
  float dist_calc2=0;
  float diflat=0;
  float diflon=0;

  //I've to spplit all the calculation in several steps. If i try to do it in a single line the arduino will explode.
  diflat=radians(flat2-flat1);
  flat1=radians(flat1);
  flat2=radians(flat2);
  diflon=radians((flon2)-(flon1));

  dist_calc = (sin(diflat/2.0)*sin(diflat/2.0));
  dist_calc2= cos(flat1);
  dist_calc2*=cos(flat2);
  dist_calc2*=sin(diflon/2.0);
  dist_calc2*=sin(diflon/2.0);
  dist_calc +=dist_calc2;

  dist_calc=(2*atan2(sqrt(dist_calc),sqrt(1.0-dist_calc)));

  dist_calc*=6371000.0; //Converting to meters
  //Serial.println(dist_calc);
  return dist_calc;

}
float UAS_gps::radianToDegree(float degree){
  return PI * degree / 180;
}
float UAS_gps::degreeToRadian(float radians){
  return 180 * radians / PI;
}

float UAS_gps::calculateAngleBetweenGPSCoordinate(float lat,float lon,float lat2,float lon2){
    float teta1 = radianToDegree(lat);
    float teta2 = radianToDegree(lat2);
    float delta1 = radianToDegree(lat2-lat);
    float delta2 = radianToDegree(lon2-lon);

    //==================Heading Formula Calculation================//

    float y = sin(delta2) * cos(teta2);
    float x = cos(teta1)*sin(teta2) - sin(teta1)*cos(teta2)*cos(delta2);
    float heading = degreeToRadian(atan2(y,x));// radians to degrees
    heading = ( ((int)heading + 360) % 360 );
    return heading;
  }

 float UAS_gps::deltaAngle(float goal_lat, float goal_lon){
    // gpsReading current_position = this->get_current_gps_location();

    // float angle_to_destination = this->calculateAngleBetweenGPSCoordinate(current_position.lat, current_position.lon, goal_lat, goal_lon);
    // angle 0 to 2pi from the x-axis
    // return current_position.heading - angle_to_destination;
    return TinyGPSPlus::courseTo(
      current_position.lat,
      current_position.lon,
      goal_lat,
      goal_lon);
 }


double UAS_gps::deltaEulerDistance(float goal_lat, float goal_lon){
    // Returns euler distance to the goal in meters

    return TinyGPSPlus::distanceBetween(
      current_position.lat,
      current_position.lon,
      goal_lat,
      goal_lon);
}

void UAS_gps::smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  //do
  //{
    //while (serial_connection->available())
    //digitalWrite(13, HIGH);
      //gps.encode(serial_connection->read());
      //serial_connection->read();
      //digitalWrite(13, LOW);
      //digitalWrite(13, HIGH);
      //gps.encode(Serial.read());
  //} while (millis() - start < ms);
}

void UAS_gps::printGPSData(float goal_lat, float goal_lon){
  Serial.println("======================");
  if(gps.location.isValid() ){
    Serial.printf("Lat: %f\n",current_position.lat);
    Serial.printf("Lon: %f\n",current_position.lon);
    Serial.printf("Alt: %f\n",current_position.alt);
    Serial.printf("Heading: %d\n",current_position.heading);
    Serial.printf("Distance to goal: %f\n", gps.distanceBetween(current_position.lat, current_position.lon, goal_lat,  goal_lon));

  }else{
    Serial.println("GPS data not valid");
  }
  Serial.println("======================");
}


double UAS_gps::getCurrentHeading(){
  return TinyGPSPlus::courseTo(last_position.lat, last_position.lon, current_position.lat, current_position.lon);
}

UAS_gps::gpsReading UAS_gps::updatePositions(){
  Serial.printf("Last position 1 %lf, %lf\n", last_position.lat, last_position.lon);
  last_position.lat = current_position.lat;
  last_position.lon = current_position.lon;
  Serial.printf("Last position 2 %lf, %lf\n", last_position.lat, last_position.lon);
  return last_position;
}
