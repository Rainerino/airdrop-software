#ifndef UAS_IMU_H
#define UAS_IMU_H

class UAS_IMU{

    public:
        UAS_IMU();
        // add some functions, like set up functions, looping functions etc
        double getCurrentHeading();
        
    private:
        // private data that others has no access to
        double current_heading;

};

#endif
