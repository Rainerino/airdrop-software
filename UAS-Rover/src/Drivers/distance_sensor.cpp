#include "distance_sensor.h"

DistanceSensor::DistanceSensor(){
}

DistanceSensor::DistanceSensor(uint8_t trig_pin, uint8_t echo_pin){
    pinMode(trig_pin, OUTPUT);
    pinMode(echo_pin, INPUT);
    this->trig_pin = trig_pin;
    this->echo_pin =echo_pin;
}

int16_t DistanceSensor::readDistanceSensor(){
    digitalWrite(trig_pin, LOW); 
    delayMicroseconds(2); 

    digitalWrite(trig_pin, HIGH);
    delayMicroseconds(10); 

    digitalWrite(trig_pin, LOW);
    uint64_t duration = pulseIn(echo_pin, HIGH);

    //Calculate the distance (in cm) based on the speed of sound.
    uint64_t distance = duration/58.2;
    int maximumRange = 200; // Maximum range needed
    int minimumRange = 0; // Minimum range needed

    if (distance >= maximumRange || distance <= minimumRange){
        return -1;
    }
    return (int16_t)distance;
}

bool DistanceSensor::checkDistanceSensorTrigger(){
    int16_t distance = -1;
    for(int i = 0 ; i < 5; i ++){
        int16_t temp_distance = readDistanceSensor();
        // Serial.println(temp_distance);
        if(temp_distance > 0 && temp_distance > distance){
            distance = temp_distance;
        }
        delay(50);
    }
    if(distance <= MINI_TRIG_DISTANCE && distance > 0){
        return true;
    }else{
        return false;
    }
}
