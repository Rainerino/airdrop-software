#ifndef DRIVERS_TELEMETRY_H
#define DRIVERS_TELEMETRY_H


// include the library: https://forum.pjrc.com/threads/28916-Teensy-3DR-Telemetry-Communication
// TODO Set up all the protocols
class Telemetry{
    public:
        /**
         * This function will be called by the lifecycle, and return the serial data if there are any, else return -1
         * 
         */ 
        
        Telemetry();

        int read_serial_data();

        bool first_stage_release;
        bool second_stage_release;

    private:

};

#endif