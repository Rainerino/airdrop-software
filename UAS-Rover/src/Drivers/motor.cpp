// // Motor.cpp

#include "motor.h"

// int LEFTMOTOR = 0;
// int RIGHTMOTOR = 1;
Motor::Motor(){
    
}
Motor::Motor(uint8_t left_in1, uint8_t left_in2, uint8_t right_in1, uint8_t right_in2){
    pinMode(left_in1, OUTPUT);
    pinMode(left_in2, OUTPUT);
    pinMode(right_in1, OUTPUT);
    pinMode(right_in2, OUTPUT);
    if (motor_direction){
        left_motor_speed_pin = left_in1;
        left_motor_direction_pin = left_in2;
        right_motor_speed_pin = right_in1;
        right_motor_direction_pin = right_in2;
    }else{
        left_motor_speed_pin = left_in2;
        left_motor_direction_pin = left_in1;
        right_motor_speed_pin = right_in2;
        right_motor_direction_pin = right_in1;
    }
    digitalWrite(left_motor_direction_pin, LOW);
    digitalWrite(right_motor_direction_pin, LOW);
}

void Motor::turnLeft(){
    analogWrite(left_motor_speed_pin, 0);
    analogWrite(right_motor_speed_pin, MOTOR_SPEED_CAP);
}

void Motor::turnRight(){
    analogWrite(right_motor_speed_pin, 0);
    analogWrite(left_motor_speed_pin, MOTOR_SPEED_CAP);
}

void Motor::goStraight(){
    analogWrite(left_motor_speed_pin, MOTOR_SPEED_CAP);
    analogWrite(right_motor_speed_pin, MOTOR_SPEED_CAP);
}

void Motor::stop(){
    analogWrite(left_motor_speed_pin, 0);
    analogWrite(right_motor_speed_pin, 0);
}

// motor::motor(uint16_t speed) {
	
// 	motorSpeedDefault = speed;
// }

// // Default parameter of 0, which runs at defualtTurnSpeed
// void motor::turnLeft() {

// }

// // Default parameter of 0, which runs at defualtTurnSpeed
// void motor::turnRight() {
	
// }

// // Default perapeter of 0, which runs motorSpeedDefault
// void motor::forward(uint16_t speed) {
// 	uint16_t forwardSpeed = (speed == 0) ? motorSpeedDefault : speed;

// 	this->speed(LEFTMOTOR, forwardSpeed);
// 	this->speed(RIGHTMOTOR, forwardSpeed);
// }


// // Default perapeter of 0, which runs motorSpeedDefault
// void motor::reverse(uint16_t speed) {
// 	uint16_t reverseSpeed = (speed == 0) ? motorSpeedDefault : speed;

// 	this->speed(LEFTMOTOR, -reverseSpeed);
// 	this->speed(RIGHTMOTOR, -reverseSpeed);
// }

// void motor::stop() {
// 	this->speed(LEFTMOTOR, 0);
// 	this->speed(RIGHTMOTOR, 0);
// }

// // Lifecycle

// /*
// // Add pid control to here
// void motor::poll() {
// 	if(runningWithPID) {
// 		int err = pid.getError();
// 		// when err < 0 turns right. when err > 0 turns left
// 		this->speed(LEFTMOTOR, motorSpeed - err);
// 		this->speed(RIGHTMOTOR, motorSpeed + err);
// 	}
// }
// */

// void motor::speed(int port, int speed){

// }