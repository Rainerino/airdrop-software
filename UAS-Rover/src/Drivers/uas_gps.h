#ifndef DRIVERS_UAS_GPS_H
#define DRIVERS_UAS_GPS_H

#include <TinyGPS++.h>
#include "../Globals.h"
#include "SoftwareSerial.h"
// #include <AltSoftSerial.h>

class UAS_gps {
    public:
        UAS_gps();
        struct gpsReading{
            double lat;
            double lon;
            double heading; // heading between 0-360
            double alt; // this is optional
        }current_position, last_position;
        // struct last_gps_position{
        //     double last_lat;
        //     double last_lon;
        // }last_position;

        /**
         * This function will return the current gps location. It will be pulled at maybe 1/3 Hz, since the reading cause delay.
         *
         *
         */

        gpsReading  get_current_gps_location();
        gpsReading updatePositions();

        /*==================================================*/
        /**
         * GPS Calulcation functions
         *
         */
        float calculateAngleBetweenGPSCoordinate(float lat,float lon,float lat2,float lon2);
        float calculateDistanceBetweenGPSCoordinate(float flat1, float flon1, float flat2, float flon2);

        float deltaAngle(float goal_lat, float goal_lon);
        double deltaEulerDistance(float goal_lat, float goal_lon);
        void printFloat(float val, bool valid, int len, int prec);
        void printGPSData(float goal_lat, float goal_lon);
        void smartDelay(unsigned long ms);

        double getCurrentHeading();
        SoftwareSerial *serial_connection;
        TinyGPSPlus gps;
        /*==================================================*/

    private:
        /*==================================================*/
        float radianToDegree(float degree);
        float degreeToRadian(float radians);
        /*==================================================*/
        // AltSoftSerial ss;
};

#endif
