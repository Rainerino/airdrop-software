#ifndef UAS_SERVO_H
#define UAS_SERVO_H

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include "../Globals.h"
#include <PWMServo.h>

class UAS_servo{
    public:
    UAS_servo();
    UAS_servo(uint8_t servo_pin);

    void run_first_stage_release();
    void run_second_stage_release();

    bool check_first_stage_completed();
    bool check_second_stage_completed();

    private: 
    PWMServo uas_servo;
    bool first_stage_released;
    bool second_stage_released;
};



#endif