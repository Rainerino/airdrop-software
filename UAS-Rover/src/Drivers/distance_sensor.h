#ifndef DRIVERS_DISTANCESENSOR_H
#define DRIVERS_DISTANCESENSOR_H

#include <stdint.h>
#include <NewPing.h>
#include "../Globals.h"

// include the ultrasonic sensor library https://forum.pjrc.com/threads/53537-Ultrasonic-Sensor-HC-SR04-Teensy-3-6-and-NewPing-library

class DistanceSensor{
    public:
        /**
         * This function will be called by the lifecycle with the max frequency. 
         * 
         * 
         */ 
        DistanceSensor();

        DistanceSensor(uint8_t trig_pin, uint8_t echo_pin);

        int16_t readDistanceSensor();

        bool checkDistanceSensorTrigger();

    private:
        uint8_t trig_pin;
        uint8_t echo_pin;
};

#endif