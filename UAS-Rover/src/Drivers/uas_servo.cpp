#include "uas_servo.h"
UAS_servo::UAS_servo(){
}

UAS_servo::UAS_servo(uint8_t servo_pin){
    uas_servo.attach(servo_pin);
    uas_servo.write(SERVO_CLOSED);
    delay(SERVO_DELAY);// for servo to get to that position;
    first_stage_released = false;
    second_stage_released = false;
}
void UAS_servo::run_first_stage_release(){
    uas_servo.write(SERVO_STAGE_1);
    delay(SERVO_DELAY);// for servo to get to that position;
    first_stage_released = true;
}
void UAS_servo::run_second_stage_release(){
    uas_servo.write(SERVO_STAGE_2);
    delay(SERVO_DELAY);// for servo to get to that position;
    second_stage_released = true;
}
bool UAS_servo::check_first_stage_completed(){
    return first_stage_released;
}
bool UAS_servo:: check_second_stage_completed(){
    return second_stage_released;
}

