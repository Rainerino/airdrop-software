#ifndef OLED_DISPLAY_H
#define OLED_DISPLAY_H

#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#define OLED_ADDR   0x3C
Adafruit_SSD1306 display(-1);

class Display{
private:

public:
    void oled_setup();
    void oled_display_message();
    void oled_display_gps_data();
    void oled_display_controller_data();
    
};



#endif