#ifndef _MOTOR_h
#define _MOTOR_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "../Globals.h"
#include <stdint.h>


class Motor {
private:
	uint8_t left_motor_speed_pin;
	uint8_t left_motor_direction_pin;
	uint8_t right_motor_speed_pin;
	uint8_t right_motor_direction_pin;
	
	bool motor_direction;
public:
	Motor();
	
	Motor(uint8_t left_in1, uint8_t left_in2, uint8_t right_in1, uint8_t right_in2);

	void attachLeftMotor( uint8_t in1, uint8_t in2);

	void attachRightMotor( uint8_t in1, uint8_t in2);

	/**
	 * Turn left by turn one of the motor off, one fully on
	 */ 
	void turnLeft();
	/**
	 * Turn right by turn one of the motor off, one fully on
	 */ 
	void turnRight();
	/**
	 * Literallto go straight
	 */ 
	void goStraight();
	
	void stop();
};

#endif