#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include <math.h>
#include <Encoder.h>
#include <PWMServo.h>
#include "Globals.h"
#include "Drivers/motor.h"
#include "Drivers/uas_gps.h"
#include "Drivers/distance_sensor.h"
#include "Drivers/telemetry.h"
#include "Drivers/uas_servo.h"
#include <uas_comm.h>
#include <Ticker.h>
#include "SoftwareSerial.h"

/*
#include "TinyGPS++.h"
#include "SoftwareSerial.h"

SoftwareSerial serial_connection2(0, 1); //RX=pin 10, TX=pin 11
TinyGPSPlus gps;//This is the GPS object that will pretty much do all the grunt work with the NMEA data
void setup()
{
  Serial.begin(9600);//This opens up communications to the Serial monitor in the Arduino IDE
  serial_connection2.begin(9600);//This opens up communications to the GPS
  Serial.println("GPS Start");//Just show to the monitor that the sketch has started
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
}

void loop()
{
  while(serial_connection2.available())//While there are characters to come from the GPS
  {
    gps.encode(serial_connection2.read());//This feeds the serial NMEA data into the library one char at a time
  }
  if(gps.location.isUpdated())//This will pretty much be fired all the time anyway but will at least reduce it to only after a package of NMEA data comes in
  {
    //Get the latest info from the gps object which it derived from the data sent by the GPS unit
    Serial.println("Satellite Count:");
    Serial.println(gps.satellites.value());
    Serial.println("Latitude:");
    Serial.println(gps.location.lat(), 6);
    Serial.println("Longitude:");
    Serial.println(gps.location.lng(), 6);
    Serial.println("Speed MPH:");
    Serial.println(gps.speed.mph());
    Serial.println("Altitude Feet:");
    Serial.println(gps.altitude.feet());
    Serial.println(gps.heading());
    Serial.println("");
  }
}
*/

SoftwareSerial serial_connection2(0, 1); //RX=pin 10, TX=pin 11
UAS_servo servo;
UAS_gps uas_gps;
TinyGPSPlus gps2;
Motor motor;
DistanceSensor ds;
comm::UASComm * xbee;


bool toggle;

void updatePositions_jump(){
  // Serial.println("In jump, going to updatePositions");
  uas_gps.updatePositions();
}
Ticker gps_resync_position(updatePositions_jump, 5000, 0, MILLIS); //1000ms interval


void commCallback(){
  xbee->commUpdateSignal();
}
Ticker comm_update(commCallback, 500, 0);

void setup() {
  // declare the ledPin as an OUTPUT:
  Serial.begin(9600);
  serial_connection2.begin(9600);
  //Serial.begin(9600); //GPS

  //set up all the pin mode
  servo = UAS_servo(SERVO_PIN);
  // Telemetry telem;
  uas_gps = UAS_gps();

  motor = Motor(LEFT_MOTOR_IN1,LEFT_MOTOR_IN2, RIGHT_MOTOR_IN1, RIGHT_MOTOR_IN2 );
  //ds = DistanceSensor(TRIG_PIN, ECHO_PIN);
  xbee = new comm::UASComm(comm::System::ROVER_SYSTEM);

  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);

  Serial.println("Set up completed");
  toggle = false;
  comm_update.start();
  gps_resync_position.start();

  servo.run_first_stage_release(); // DELETE
  servo.run_second_stage_release(); // DELETE

}

/**
 * The controller module
 *
 *
 */
 
constexpr float ugv_rotate_rate = 1000 / 90;  // milliseconds / degree
constexpr float ugv_forward_rate = 1000 / 1;  // milliseconds / m
void make_move_step(UAS_gps gps, Motor motor){
  // TODO this is yet to be tested
  // gps.get_current_gps_location();
  Serial.println("Startıng move...");

  float angle_difference = gps.deltaAngle(GOAL_LAT, GOAL_LON);
  bool wait = true;
  if(angle_difference < 180){
    Serial.println("Turnıng rıght");
    motor.turnRight();
  } else if(angle_difference > 180){
    Serial.println("Turnıng left");
    motor.turnLeft();
  } else {
    // Everything is good! For now
    wait = false;
  }

  if (wait) {
    Serial.println("Waitıng to turn...");
    delay(static_cast<int>(ugv_rotate_rate * angle_difference));
  }
  motor.stop();

  double distance_to_goal = gps.deltaEulerDistance(GOAL_LAT, GOAL_LON);
  int time_driving = static_cast<int>(ugv_forward_rate * distance_to_goal);

  if(time_driving > 5000) {
    time_driving = 5000;
  }

  motor.goStraight();
  Serial.println("Drivıng forward...");
  //Serial.println("Distance forward...");
  Serial.println(distance_to_goal);
  delay(time_driving);
  motor.stop();
  Serial.println("Done move step");
  Serial.println(uas_gps.gps.location.lat());
}


void loop(){

  while(serial_connection2.available())//While there are characters to come from the GPS
  {
    gps2.encode(serial_connection2.read());//This feeds the serial NMEA data into the library one char at a time
  }
  //if(gps2.location.isUpdated())//This will pretty much be fired all the time anyway but will at least reduce it to only after a package of NMEA data comes in
  //{
    //Get the latest info from the gps object which it derived from the data sent by the GPS unit
    Serial.println("Satellite Count:");
    Serial.println(gps2.satellites.value());
    Serial.println("Latitude:");
    Serial.println(gps2.location.lat(), 6);
    Serial.println("Longitude:");
    Serial.println(gps2.location.lng(), 6);
    Serial.println("Speed MPH:");
    Serial.println(gps2.speed.mph());
    Serial.println("Altitude Feet:");
    Serial.println(gps2.altitude.feet());
    //Serial.println(gps2.heading());
    Serial.println("");
  //}



  uas_gps.get_current_gps_location();
  gps_resync_position.update(); //this will check the Ticker and if necessary, it will run the callback function
  // Serial.printf("=========Heading is: %lf\n",uas_gps.calculateAngleBetweenGPSCoordinate());
  Serial.printf("Location is: %lf %lf\n", uas_gps.current_position.lat, uas_gps.current_position.lon);
  Serial.printf("Heading is: %lf\n",TinyGPSPlus::courseTo(
    uas_gps.current_position.lat,
    uas_gps.current_position.lon,
    GOAL_LAT,
    GOAL_LON));

  Serial.printf("========= Last Heading is: %lf\n",TinyGPSPlus::courseTo(
    uas_gps.current_position.lat,
    uas_gps.current_position.lon,
    uas_gps.last_position.lat,
    uas_gps.last_position.lon));

  int IR = analogRead(A0);

  Serial.println(IR);

  comm_update.update();
  if (xbee->current_signal == comm::Flags::WINCH_DROP_TRIGGERED && !servo.check_first_stage_completed() && !servo.check_second_stage_completed()){
    servo.run_first_stage_release();
    Serial.println("Stage 1 triggered");
    // loop speed 1
  }else if(servo.check_first_stage_completed() && !servo.check_second_stage_completed() && IR > 14 ){
    servo.run_second_stage_release();
    Serial.println("Stage 2 triggered");
    xbee->setRoverReleasedStage();
    // loop speed 2
  }else if(servo.check_first_stage_completed() && servo.check_second_stage_completed()){
    Serial.println("Stage 3 running");
    //digitalWrite(13, HIGH);
    //make_move_step(uas_gps, motor);

    // loop speed 3
  }else{
    delay(LOOP_IDLE);
    digitalWrite(13, toggle);
    toggle = !toggle;
    Serial.println("Looping");
  }
}
