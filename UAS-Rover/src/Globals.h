// Globals.h
#ifndef _GLOBALS_h
#define _GLOBALS_h

#include <stdint.h>

extern double GOAL_LAT;
extern double GOAL_LON;
extern double TURN_ANGLE_MIN;
extern double GOAL_STOP_DISTANCE_RADIUS;


extern uint16_t MOTOR_SPEED_CAP;

extern uint8_t LEFT_MOTOR_IN1 ;
extern uint8_t LEFT_MOTOR_IN2 ;
extern uint8_t RIGHT_MOTOR_IN1 ;
extern uint8_t RIGHT_MOTOR_IN2 ;

extern uint8_t GPS_PIN_1;
extern uint8_t GPS_PIN_2;
extern uint8_t SERVO_PIN;

extern uint8_t SERVO_CLOSED;
extern uint8_t SERVO_STAGE_1;
extern uint8_t SERVO_STAGE_2;
extern uint8_t SERVO_DELAY;

extern uint32_t GPS_BAUD;

extern uint8_t TRIG_PIN;
extern uint8_t ECHO_PIN;
extern uint16_t MINI_TRIG_DISTANCE;
extern uint64_t LOOP_DELAY_1 ;
extern uint64_t LOOP_DELAY_2 ;
extern uint64_t LOOP_DELAY_3 ;
extern uint64_t LOOP_IDLE;
extern uint64_t GPS_DELAY;
#endif